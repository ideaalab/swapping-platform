/* ------------------------------------------------------------ */
/*                                                              */
/*        Created by Martin Andersen at IDEAA Lab               */
/*               2020 - http://ideaalab.com                     */
/*                                                              */
/* Full project, expanation and videos here:                    */
/* https://ideaalab.com/plataforma-cambio                       */
/*                                                              */
/* Release under Creative Commons Atribution-ShareAlike license */
/* http://creativecommons.org/licenses/by-sa/4.0/               */
/*                                                              */
/* ------------------------------------------------------------ */

/* CONECTIONS */
#define MOTOR_A   A0  //IN1 ULN2003
#define MOTOR_B   A1  //IN2 ULN2003
#define MOTOR_C   A2  //IN3 ULN2003
#define MOTOR_D   A3  //IN4 ULN2003

#define BTN       4   //action button

/* CONFIGURATION */
//uncomment which phase configuration you want to use
//#define ONE_PHASE     //one winding activated each time. Normal configuration. > Choose this for lowest power consumption <
#define TWO_PHASE       //two windings activated at the same time. 41% more torque and double power consumption. > Choose this for more torque <
//#define HALF_PHASE    //one and two windings activated alternally. Smoother move, mid point between the previous. > Chose this for smoother move <

#define MOTOR_SPEED     2000  //higher value => slower movement. If it doesnt move try higher value
#define ROTATE_DEGREES  180   //angle to rotate. Positive values rotate clockwise, negative value rotate counter clockwise
#define USE_RF          true  //if using regular push button, use "false", if using RF digital receiver use "true"

/* OTHER DEFINES (DONT TOUCH!) */
#define STEPS_PER_REV 2038
#define MS_DEBOUNCE   20

//define if BTN_PRESSED is active high or low
#if USE_RF == false
//buttons pressed is active low
#define BTN_PRESSED   false
#else
//RF pressed is active high
#define BTN_PRESSED   true
#endif

/* VARIABLES */
int stepCounter = 0;   //step counter

#if defined(ONE_PHASE)
#define NUM_STEPS           4
#define MINI_STEPS_PER_REV  STEPS_PER_REV
const int stepperOutput[NUM_STEPS] = {B0001, B0010, B0100, B1000};  //lookup table for the motor output
#elif defined(TWO_PHASE)
#define NUM_STEPS           4
#define MINI_STEPS_PER_REV  STEPS_PER_REV
const int stepperOutput[NUM_STEPS] = {B0011, B0110, B1100, B1001};  //lookup table for the motor output
#elif defined(HALF_PHASE)
#define NUM_STEPS           8
#define MINI_STEPS_PER_REV  (STEPS_PER_REV * 2)
const int stepperOutput[NUM_STEPS] = {B0001, B0011, B0010, B0110, B0100, B1100, B1000, B1001};  //lookup table for the motor output
#endif

/*
 * Configuration of the project is done in the setup() funcion
 */
void setup(void){
  Serial.begin(9600);
  
  //declare motor pins as output
  pinMode(MOTOR_A, OUTPUT);
  pinMode(MOTOR_B, OUTPUT);
  pinMode(MOTOR_C, OUTPUT);
  pinMode(MOTOR_D, OUTPUT);

  //declare button pin as input
#if USE_RF == false
  //for buttons we use pullups
  pinMode(BTN, INPUT_PULLUP);
#else
  //for RF dont need pullups
  pinMode(BTN, INPUT);
#endif
}

/*
 * Main code is here and will run in a loop
 */
void loop(void){
  if(digitalRead(BTN) == BTN_PRESSED){
    turnDegrees(ROTATE_DEGREES);

    //wait so it can not be triggered again until the button is released
    while(digitalRead(BTN) == BTN_PRESSED){delay(MS_DEBOUNCE);}
  }
}

/*
 * Convert degrees into steps and move the stepper
 * Positive degrees move motor clockwise
 * Negative degrees move the motor counter clockwise
 * Degrees higher than 360 will move the motor more than one turn
 * For ex:
 *  turnDegrees(-720);  //this will make 2 turns CCW
 */
void turnDegrees(int deg){
  bool negativeValue = false;
  long stepsToMove;

  //check if we need to move CW or CCW
  if(deg < 0){
    deg = deg * -1; //convert degrees to positive
    negativeValue = true;
  }

  //calculate steps to move
  stepsToMove = ((long)deg * MINI_STEPS_PER_REV) / 360;

  //print to serial, not needed but nice to see
  Serial.print("Moving ");
  Serial.print(deg);
  Serial.print("º ");
  if(negativeValue == false){Serial.print("CW");}
  else{Serial.print("CCW");}
  Serial.print(" -> ");
  if(negativeValue == true){Serial.print("-");}
  Serial.print(stepsToMove);
  Serial.println(" steps"); 

  //move stepper
  for(int x = 0; x < stepsToMove; x++){
    if(negativeValue == false){
      stepCounter++;
      
      if(stepCounter == NUM_STEPS){
        stepCounter = 0;
      }
    }
    else{
      stepCounter--;
      
      if(stepCounter == -1){
        stepCounter = NUM_STEPS - 1;
      }
    }
    
    setOutput();
    delayMicroseconds(MOTOR_SPEED);
  }

  motorOff(); //turn off the motor
}

/*
 * Set the motor output according to the step we are in
 */
void setOutput(void){
  digitalWrite(MOTOR_A, bitRead(stepperOutput[stepCounter], 0));
  digitalWrite(MOTOR_B, bitRead(stepperOutput[stepCounter], 1));
  digitalWrite(MOTOR_C, bitRead(stepperOutput[stepCounter], 2));
  digitalWrite(MOTOR_D, bitRead(stepperOutput[stepCounter], 3));
}

/*
 * Turn off the motor to reduce power consumption
 */
void motorOff(void){
  digitalWrite(MOTOR_A, LOW);
  digitalWrite(MOTOR_B, LOW);
  digitalWrite(MOTOR_C, LOW);
  digitalWrite(MOTOR_D, LOW);
}
